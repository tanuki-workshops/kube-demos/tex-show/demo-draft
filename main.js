
var counter1 = 0
var counter2 = 0

function message() {
  return "👋 🌍"
}

function randomInteger(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function main(params) {
  
  counter1+=1
  counter2=randomInteger(0,25)

  let text =  "👋 Tex Show Rocks"
  console.log(text)

  eval(text)

  return JSON.stringify({
    message: message(),
    total: 42,
    authors: ["@k33g_org"],
    context: "this is a demo",
    params: params.getString("name"),
    revision: text,
    counter1: counter1,
    counter2: counter2
  })
}

function metrics() {
  
  let results = `
    # HELP small_app_counter: call counter function
    # TYPE small_app_counter counter
    small_app_counter ${counter1}
    # HELP other_app_counter: call counter function
    # TYPE other_app_counter counter
    other_app_counter ${counter2}  
  `
  return results
}

function index() {
  return `
    <!doctype html>
    <html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      <title>👋 World</title>
      <meta name="description" content="">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <style>
        .container
        {
          min-height: 100vh;
          display: flex;
          justify-content: center;
          align-items: center;
          text-align: center;
        }
        .title
        {
          font-family: "Source Sans Pro", "Helvetica Neue", Arial, sans-serif;
          display: block;
          font-weight: 300;
          font-size: 100px;
          color: #35495e;
          letter-spacing: 1px;
        }
      </style>
    </head>
    <body>
      <section class="container">
          <h1 class="title">
          👋 this is the <version></version> version
          </h1>
      </section>
    </body>
    <script>
      fetch('/', {
        method: 'post',
        body: JSON.stringify({name:"Bob Morane"}),
        headers : { 
          "Content-Type": "application/json",
          "Accept": "application/json"
        }
      })
      .then(response => response.text())
      .then(text => {
        console.log(text)
        document.querySelector("version").innerHTML=JSON.parse(text).revision
      })

      let interval = setInterval(myTimer, 5000);

      function myTimer() {
        fetch('/', {
          method: 'post',
          body: JSON.stringify({name:"Jane Doe"}),
          headers : { 
            "Content-Type": "application/json",
            "Accept": "application/json"
          }
        })
        .then(response => response.text())
        .then(text => {
          console.log(JSON.parse(text))
        })
      }

    </script>
    </html>
  `
}

